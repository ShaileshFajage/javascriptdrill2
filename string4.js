

function getFullName(obj) {

    let str = "";

    if(obj.hasOwnProperty('first_name'))
    {
        str+=obj['first_name']+" ";
    }

    if(obj.hasOwnProperty('middle_name'))
    {
        str+=obj['middle_name']+" ";
    }

    if(obj.hasOwnProperty('last_name'))
    {
        str+=obj['last_name'];
    }

    return str;
    
}

module.exports = getFullName;